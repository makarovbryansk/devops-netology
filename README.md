# devops-netology

##First Lesson

В gitignore мы описали файлы которые будем игнорировать:

* файлы с расширением tfstate и файлы которые будут иметь структуру в имени .tfstate.

* логи крашей

* файлы с расширением tfvars

* override.tf

* override.tf.json

* *_override.tf

* *_override.tf.json

* .terraformrc

* terraform.rc

>Вобщем все файлы которые имеют значение только для нас локально.